import { getPageUrl } from '../../utils';
import { getEnv } from '../../configs/env';
import { ProductDetail } from '../../models/product-detail-page';

fixture`Product`.beforeEach(t => t.resizeWindow(1280, 800)); // Work around to fix click issue in headless mode

const { url } = getEnv();
const productDetailPage = new ProductDetail();

test('Can see correct variants in product detail page', async t => {
  const productUrl = 'dong-ho-nam-thep-cao-cap-1370111.html';
  // const productUrl = 'set-bo-cao-cap-1363371.html';
  await t
    .navigateTo(`${url}/${productUrl}`)
    .expect(getPageUrl())
    .contains(productUrl);
  const options = productDetailPage.productsOptions;
  await t.expect(options.count).eql(2);
});
