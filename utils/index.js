import { ClientFunction } from "testcafe";

export const getPageUrl = ClientFunction(() => window.location.href);

export const getPageSearch = ClientFunction(() => window.location.search);

export const getPageTitle = ClientFunction(() => document.title);

export const clearLS = ClientFunction(key => localStorage.removeItem(key));

export const getLS = ClientFunction(key =>
  JSON.parse(localStorage.getItem(key))
);

export const setLS = ClientFunction((key, value) =>
  localStorage.setItem(key, JSON.stringify(value))
);

export const reload = ClientFunction(() => location.reload(false));
