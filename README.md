# E2e test for FE TTS UI

## Installation

```bash
npm install
```

## Usage

There are 2 enviroments: `sandbox` and `prod`

There is 1 app (until now): `desktop`

Run

```bash
npm run test:desktop:headless:sandbox
```

for testing `desktop` app with `sandbox` environment in `headless` mode

See `package.json` for more scripts.
