import minimist from 'minimist';

const args = minimist(process.argv.slice(2));

const envs = {
  desktop: [
    {
      env: 'sandbox',
      url: 'https://web-v2.thitruongsi.com',
    },
    {
      env: 'prod',
      url: 'https://thitruongsi.com',
    },
  ],
};

export const getEnv = () => {
  return envs[args.prj].find(env => env.env === args.env);
};
