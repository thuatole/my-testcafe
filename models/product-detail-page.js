import { ReactSelector } from 'testcafe-react-selectors';

export class ProductDetail {
  constructor() {
    this.productsOptions = ReactSelector('Tag').withProps({
      paddingY: 6,
    });
  }
}
